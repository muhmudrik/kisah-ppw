from django.shortcuts import render

# Create your views here.
def my_home(request):
    return render(request, "home.html")

def error404(request, exception):
    return render(request, '404.html')

def my_404(request):
    return render(request, '404.html')