from . import models
from django import forms
from datetime import datetime, date

class JadwalForm(forms.Form):
    date = forms.DateField(label='date', 
                           required=True, 
                           widget=forms.SelectDateWidget(attrs={'placeholder': '13:00', 'class':'form-control timepicker'})
                           )
    waktu = forms.TimeField(label='waktu', 
                            required=True, 
                            widget=forms.TimeInput(attrs={'placeholder': '13:00', 'class':'form-control'})
                            )
    kegiatan = forms.CharField(label='kegiatan', 
                            max_length=40, 
                            required=True, 
                            widget=forms.TextInput(attrs={'placeholder': 'Kegiatan', 'class':'form-control'})
                            )
    tempat = forms.CharField(label='tempat',
                            max_length=60, 
                            required=True, 
                            widget=forms.TextInput(attrs={'placeholder': 'Lokasi', 'class':'form-control'})
                            )
    kategori = forms.CharField(label='kategori', 
                            max_length=30, 
                            required=True, 
                            widget=forms.TextInput(attrs={'placeholder': 'Kategori Kegiatan', 'class':'form-control'})
                            )
