from django.shortcuts import render, redirect
from .models import Jadwal
from .forms import JadwalForm
from django.http.response import HttpResponseRedirect
from datetime import datetime

# Create your views here.
def jadwal(request):
    # cek method post
    # hari, tanggal, jam, nama kegiatan, tempat, dan kategori.
    # 
    form = JadwalForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        # print(form.data)
        # print('masuk if')

        jadwal = Jadwal(
            date=datetime.strptime('{} {} {} {}'.format(form.data['date_month'], form.data['date_day'], form.data['date_year'], form.data['waktu']), '%m %d %Y %H:%M'),
            # waktu=form.data['waktu'],
            kegiatan=form.data['kegiatan'],
            tempat=form.data['tempat'],
            kategori=form.data['kategori'],
        )
        # print(Jadwal.objects.all().values())
        # print('sini ancol   ')
        jadwal.save()
    form=JadwalForm()
    jadwal=Jadwal.objects.order_by('date')
    # print(Jadwal.objects.all())
    return render(request, "jadwal.html", {'form': form, 'jadwal':jadwal})
    # else:
    #     HttpResponseRedirect('/schedule')

def delete(request):
    if request.method == "POST" and 'id' in request.POST :
        id = request.POST['id']
        Jadwal.objects.get(id=id).delete()
    return redirect('/schedule/')
