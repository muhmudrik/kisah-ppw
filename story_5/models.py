from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
# hari, tanggal, jam, nama kegiatan, tempat, dan kategori.
class Jadwal(models.Model):
    date = models.DateTimeField(null=True)
    # waktu= models.TimeField(default=timezone.now())
    kegiatan = models.CharField(max_length=100)
    tempat = models.CharField(max_length=100)
    kategori = models.CharField(max_length=100)

    # ini komentar
    