$("body").addClass("mode2");

// Accordion part
$(document).ready(function () {
    $(".atribut > button").click(function () {
        console.log($(this).siblings(".isi"));
        $(this).siblings(".isi").slideToggle("fast");
    });
});

//Theme Part
$(document).ready(function () {
    $('input:checkbox').change(function(){
        if($(this).is(':checked')){
            $("body").removeClass("mode2");
            $("body").addClass("mode1");

            $(".container .mode1").addClass("mode2");
            $(".container .mode1").removeClass("mode1");
            $('input:checkbox').prop('checked', true);
        }
        else{
            $("body").removeClass("mode1");
            $("body").addClass("mode2");

            $(".container .mode2").addClass("mode1");
            $(".container .mode2").removeClass("mode2");
            $('input:checkbox').prop('checked', false);
        }
    });
});
