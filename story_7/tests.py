from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from.views import profilku


# Create your tests here.
class TestProfilJS(TestCase):
    def test_url_story7_ada(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_coba_url_aneh_aneh(self):
        response = Client().get('/tidurku-hilang-kemana/')
        self.assertIn("404", response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
    
    def test_story6_make_fungsi_profil(self):
        found=resolve('/story7/')
        self.assertEqual(found.func, profilku)
        