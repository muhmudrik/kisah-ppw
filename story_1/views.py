from django.shortcuts import render

# Create your views here.
def my_prof(request):
    return render(request, "my_profile.html")
