from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import bukuku

# Create your tests here.
class TestTampilBuku(TestCase):
    def test_url_story8_ada(self):
        response = Client().get('/buku/')
        self.assertEqual(response.status_code, 200)

    def test_coba_url_aneh_aneh(self):
        response = Client().get('/tidurku-hilang-kemana/')
        self.assertIn("404", response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)

    def test_make_fungsi_bukuku(self):
        found = resolve('/buku/')
        self.assertEqual(found.func, bukuku)

    def test_template_html_kepake(self):
        response = Client().get('/buku/')
        self.assertTemplateUsed(response, "daftarBuku.html")