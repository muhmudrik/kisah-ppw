$.ajax({
    method: "GET",
    url: "https://www.googleapis.com/books/v1/volumes?q=naruto",
    success: function(books){
        console.log(books);
        $("#tableItem").empty();
        $("#tableItem").append(
            "<thead class='table-success'>" +
                "<th scope='col'>No</th>" + 
                "<th scope='col'>Book Title</th>" +
                "<th scope='col'>Author</th>" +
                "<th scope='col'>Description</th>" +
                "<th scope='col'>Cover</th>" +
            "</thead>" + "<tbody>"
        );
        if(books.totalItems>0){
            for(i=0; i<books.items.length; i++){
                $("#tableItem").append(
                    "<tr>" + 
                        "<td>" + (i+1) + "</td>" +
                        "<td> <p>" + books.items[i].volumeInfo.title + "</p> </td>" +
                        "<td>" + "<ul>"+ books.items[i].volumeInfo.authors.map((obj) => {return `<li>${obj}</li>`}).join("") + + (books.items[i].volumeInfo.authors ? books.items[i].volumeInfo.authors : "No Authors known" ) + "</ul>" + "</td>" +
                        "<td> <p>" + (books.items[i].volumeInfo.description ? books.items[i].volumeInfo.description : "No Description available" ) + "</p> </td>" +
                        "<td><img style='width: 128px; height: 182px;' src='" + (books.items[i].volumeInfo.imageLinks ? books.items[i].volumeInfo.imageLinks.thumbnail : "https://rpmcity.com/wp-content/themes/Aruna/img/missing_img.png" ) + "'></img></td>"+
                    "</tr>" 
                );
            }
        }
        else{
            $("#tableItem").append("<p>Sorry, no books found</p>")
        }
        $("#tableItem").append("</tbody>")
    }
})


function angkatKey(e) { 
    var keyInput = $("#inputNamaBuku").val();
    console.log("BERHASIL YEY");
    console.log(keyInput)
    $.ajax({
        method: "GET",
        url: "https://www.googleapis.com/books/v1/volumes?q=" + keyInput,
        success: function(books){
            console.log(books);
            $("#tableItem").empty();

            if(books.totalItems>0){
                $("#tableItem").append(
                    "<thead class='table-success'>" +
                        "<th scope='col'>No</th>" + 
                        "<th scope='col'>Book Title</th>" +
                        "<th scope='col'>Author</th>" +
                        "<th scope='col'>Description</th>" +
                        "<th scope='col'>Cover</th>" +
                    "</thead>"
                );
                for(i=0; i<books.items.length; i++){
                    $("#tableItem").append(
                        "<tbody><tr>" + 
                            "<td>" + (i+1) + "</td>" +
                            "<td> <p>" + books.items[i].volumeInfo.title + "</p> </td>" +
                            "<td>" + "<ul>"+ (books.items[i].volumeInfo.authors ? books.items[i].volumeInfo.authors.map((obj) => {return `<li>${obj}</li>`}).join("") : "No Authors known" ) + "</ul>" + "</td>" +
                            "<td> <p>" + (books.items[i].volumeInfo.description ? books.items[i].volumeInfo.description : "No Description available" ) + "</p> </td>" +
                            "<td><img style='width: 128px; height: 182px;' src='" + (books.items[i].volumeInfo.imageLinks ? books.items[i].volumeInfo.imageLinks.thumbnail : "https://rpmcity.com/wp-content/themes/Aruna/img/missing_img.png" ) + "'></img></td>"+
                        "</tr>" 
                    );
                }
                $("#tableItem").append("</tbody>")
            }
            else{
                $("#tableItem").append("<p>Sorry, no books found</p>")
            }
        }
    })
};