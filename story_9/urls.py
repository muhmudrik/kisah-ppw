from django.urls import path
from.views import *

urlpatterns = [
    path('', landingPage),
    path('login/', terlogin),
    path('logout/', terlogout),
]