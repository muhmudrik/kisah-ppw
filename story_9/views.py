from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout

# Create your views here.
def landingPage(request):
    return render(request, "landingPage.html")

def terlogin(request):
    pengguna=request.user
    if pengguna.is_authenticated:
        return redirect('/user/')

    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        # print(request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/user/')
    else:
        form = AuthenticationForm()

    return render(request, "loginPage.html", {'form':form})

def terlogout(req):
    logout(req)
    return redirect('/user/')
