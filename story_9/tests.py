from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from.views import *
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User


# Create your tests here.
class TestLogin(TestCase):
    def test_url_story9_ada(self):
        response = Client().get('/user/')
        self.assertEqual(response.status_code, 200)

    def test_coba_url_aneh_aneh(self):
        response = Client().get('/tidurku-hilang-kemana/')
        self.assertIn("404", response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
    
    def test_story6_make_fungsi_profil(self):
        found=resolve('/user/')
        self.assertEqual(found.func, landingPage)

    def test_url_login_ada(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code, 200)

class Story9FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        User.objects.create_user('ruka', '' , 'cobacoba')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story9FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story9FunctionalTest, self).tearDown()

    # def test_tampilan_awal(self):
    #     selenium=self.selenium
    #     selenium.get(self.live_server_url)
    #     title = selenium.find_element_by_id('halo')

    def test_hal_login(self):
        self.selenium.get(self.live_server_url+"/user/login/")
        self.assertIn('<form class="form-signin" method="POST" action="/user/login/">', self.selenium.page_source)

    def test_login(self):
        self.selenium.get(self.live_server_url+"/user/login/")
        user = self.selenium.find_element_by_id("id_username")
        password = self.selenium.find_element_by_id("id_password")
        login = self.selenium.find_element_by_id("login")
        user.send_keys("ruka")
        password.send_keys("cobacoba")
        login.click()
        time.sleep(8)
        self.assertIn("<h3> Halo ruka</h3>", self.selenium.page_source)

    def test_logout(self):
        self.selenium.get(self.live_server_url+"/user/login/")
        user = self.selenium.find_element_by_id("id_username")
        password = self.selenium.find_element_by_id("id_password")
        login = self.selenium.find_element_by_id("login")
        user.send_keys("ruka")
        password.send_keys("cobacoba")
        login.click()
        self.assertIn("<h3> Halo ruka</h3>", self.selenium.page_source)
        logout = self.selenium.find_element_by_id("logout")
        logout.click()
        time.sleep(2)
        self.assertIn('<h3>Eh kamu siapa? <br> login dulu.</h3>', self.selenium.page_source)
